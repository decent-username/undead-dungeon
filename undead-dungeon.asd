;;;; undead-dungeon.asd

(asdf:defsystem #:undead-dungeon
  :description "Describe undead-dungeon here"
  :author "decent-username"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :depends-on (#:trivial-gamekit
               #:dungen)
  :pathname "src/"
  :components ((:file "package")
               (:file "math")
               (:file "camera")
               (:file "util")
               (:file "sprite")
               (:file "tile")
               (:file "player")
               (:file "enemy")

               (:file "animation")
               (:file "animation-player")
               (:file "main")))
