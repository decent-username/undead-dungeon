(in-package #:dungen)

(defun filter-carvable (kernel)
  (not (kernel-detect kernel #'carved-p)))

(defun choose-corridor-cell (stage cells)
  (let ((rng (state-rng *state*)))
    (if (> (random-float rng) (stage-wild-factor stage))
        (random-element rng cells)
        (first cells))))

(defun choose-corridor-direction (kernel)
  (let (results)
    (dolist (dir '((0 1) (0 -1) (1 0) (-1 0)))
      (a:when-let ((cell1 (apply #'select kernel dir))
                   (cell2 (apply #'select kernel (mapcar #'+ dir dir))))
        (unless (carved-p cell2)
          (push (list cell1 cell2) results))))
    (random-element (state-rng *state*) results)))

(defun carve-direction (kernel cells)
  (let ((origin (select kernel 0 0)))
    (a:if-let ((choice (choose-corridor-direction kernel)))
      (loop :for cell :in choice
            :do (carve cell :corridor)
            :finally (return (push cell cells)))
      (progn
        (push origin (state-dead-ends *state*))
        (delete origin cells :count 1)))))

(defun carve-corridor-cell (kernel)
  (let ((stage (kernel-stage kernel))
        (origin (select kernel 0 0))
        (layout (layout :orthogonal :max-x 2 :max-y 2)))
    (make-region)
    (carve origin :corridor)
    (labels ((recurse (cells)
               (when cells
                 (let* ((cell (choose-corridor-cell stage cells))
                        (kernel (cell->kernel stage cell layout)))
                   (recurse (carve-direction kernel cells))))))
      (recurse (list origin)))))

(defun carve-corridors (stage)
  (convolve stage (layout :rectangle) #'filter-carvable #'carve-corridor-cell))

(defun filter-dead-end (kernel)
  (let ((dirs (count nil (kernel-map kernel #'carved-p))))
    (and (carved-p (select kernel 0 0))
         (> dirs 2))))

(defun erode-dead-end (kernel)
  (uncarve (select kernel 0 0))
  (remove-connectors kernel)
  (kernel-detect kernel (lambda (x) (when (carved-p x) x))))

(defun erode-dead-ends (stage)
  (process stage nil #'filter-dead-end #'erode-dead-end
           :items (state-dead-ends *state*)
           :generator (lambda (x) (cell->kernel stage x (layout :orthogonal)))))

;;;;----------------------------------------------------------------------------
;;;; ADDED BY DECENT-USERNAME
;;;;----------------------------------------------------------------------------
(defun add-wall-features (stage)
  (with-accessors ((grid stage-grid)
                   (width stage-width)
                   (height stage-height))
      stage
    (loop :for x :from 0 :below width :do
         (loop :for y :from 0 :below height :do
              (when (has-feature-p (aref grid x y) :wall)
                (setf (cell-features (aref grid x y))
                      (cons (find-wall-features (aref grid x y) stage)
                            (cell-features (aref grid x y)))))))))

;;;;----------------------------------------------------------------------------
(defun find-wall-features (cell stage)
  "this function returns an 8D vector which
   shows which of the surronding tiles are floor"
  ;; check all surrounding tiles
  (with-accessors ((grid stage-grid)
                   (width stage-width)
                   (height stage-height))
      stage
   (let ((temp-vec9 (make-array 9 :fill-pointer 0))
         (global-cell-x (cell-x cell))
         (global-cell-y (cell-y cell)))
     ;; nil inside `result-vec8' indicates a wall
     (loop :for x :from -1 :to 1 :do
          (loop :for y :from -1 :to 1 :do
               (let* ((global-x (+ x global-cell-x))
                      (global-y (+ y global-cell-y))
                      features)

                 (if (not (or (or (< global-x 0) (>= global-x width))
                              (or (< global-y 0) (>= global-y height))))
                      (progn
                        (setf features
                              (cell-features (aref grid global-x global-y)))
                       (if (not (member :wall features))
                           (vector-push t temp-vec9)
                           (vector-push nil temp-vec9)))

                      (vector-push nil temp-vec9)))))
     (feature-vec9->feature-vec8 temp-vec9))))

;;;;----------------------------------------------------------------------------
(defun feature-vec9->feature-vec8 (vec9)
  "2 5 8     0 1 2
   1 4 7 --> 3 - 4
   0 3 6     5 6 7
             ^ result-vec that represents these cells"
  (let ((result-vec8 (make-array 8 :fill-pointer 0)))
    ;; there's probably some smarter way of doing this
    (vector-push (aref vec9 2) result-vec8)
    (vector-push (aref vec9 5) result-vec8)
    (vector-push (aref vec9 8) result-vec8)
    (vector-push (aref vec9 1) result-vec8)
    (vector-push (aref vec9 7) result-vec8)
    (vector-push (aref vec9 0) result-vec8)
    (vector-push (aref vec9 3) result-vec8)
    (vector-push (aref vec9 6) result-vec8)

    result-vec8))
