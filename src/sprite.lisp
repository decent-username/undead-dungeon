;;;; sprite.lisp
(in-package #:undead-dungeon)


(defclass sprite ()
  ((resource-keyword
    :initarg :resource-keyword
    :accessor resource-keyword
    :initform (error "Every sprite needs a resource keyword"))))


(defclass sprite-sheet-sprite (sprite)
  ((x :accessor x :initarg :x
      :initform (error "Every sprite-sheet-sprite needs an x-offset `x'."))
   (y :accessor y :initarg :y
      :initform (error "Every sprite-sheet-sprite needs a y-offset `y'."))
   (w :accessor w :initarg :w
      :initform (error "Every sprite-sheet-sprite needs a width `w'."))
   (h :accessor h :initarg :h
      :initform (error "Every sprite-sheet-sprite needs a height `h'."))
   (tile-size :accessor tile-size
	      :initarg :tile-size
	      :initform (error "Every sprite-sheet-sprite needs a `tile-size'."))
   (resource-keyword :initarg :sprite-sheet
		     :accessor sprite-sheet
		     :initform (error "Every sprite-sheet-sprite needs a `sprite-sheet'.")
		     :documentation "This is a keyword, which specifies which resource to refer to.")))

;; (defmethod initialize-instance :after ((sss sprite-sheet-sprite) &key)
;;   (setf (resource-keyword sss) (sprite-sheet sss)))


(defun make-sprite-sheet-sprite (x y w h tile-size sprite-sheet)
  (make-instance 'sprite-sheet-sprite
		 :x x :y y :w w :h h
		 :tile-size tile-size
		 :sprite-sheet sprite-sheet))


(defmethod draw-sprite ((sss sprite-sheet-sprite) position (c camera))
  "`position' is a gamekit:vec2"
  (with-accessors ((x x)
		   (y y)
		   (w w)
		   (h h)
		   (ts tile-size)
		   (ss sprite-sheet))
      sss
    (gamekit:with-pushed-canvas ()
      (gamekit:scale-canvas *window-scale* *window-scale*)

      (gamekit:draw-image position
			  ss
			  :origin (gamekit:vec2 (* x ts) (* y ts))
			  :width  (* w ts)
			  :height (* h ts)))))
