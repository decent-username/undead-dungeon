;;;; utils.lisp
(in-package #:undead-dungeon)


(defun clear-gamekit-bindings ()
  (loop :for button :in (list :left :right :up :down :enter) ; perhaps collect the bindings we've made, instead.
     :do
       (gamekit:bind-button button :pressed nil)
       (gamekit:bind-button button :released nil)))


(defun now ()
  "Return seconds since certain point of time"
  (/ (get-internal-real-time) internal-time-units-per-second))


(defclass positionable ()
  ((global-pos :accessor global-pos :initarg :global-pos :initform *window-origin*))
  (:documentation "every positionable has global x and y coordinates"))


(defmethod move-to! ((p positionable) new-pos)
  "`new-pos' is a gamekit:vec2"
  (setf (global-pos p) new-pos))


(defun vec2-floor (vec)
  (let ((x (gamekit:x vec))
        (y (gamekit:y vec)))
    (gamekit:vec2 (floor x) (floor y))))


(defun print-dungeon (&optional (stage *dungeon*))
  (format t "~&")
  (loop :for y :from (1- (dungen::stage-height stage)) :downto 0
     :do (loop :for x :below (dungen::stage-width stage)
            :for cell = (dungen::get-cell stage x y)
            :do (format t "~a"
                        (cond ((dungen::has-feature-p cell :door/horizontal)
                               "──")
                              ((dungen::has-feature-p cell :door/vertical)
                               "│ ")
                              ((dungen::has-feature-p cell :stairs/up)
                               "↑↑")
                              ((dungen::has-feature-p cell :stairs/down)
                               "↓↓")
                              ((or (dungen::has-feature-p cell :room)
                                   (dungen::has-feature-p cell :corridor))
                               "  ")
                              ((dungen::has-feature-p cell :wall)
                               "██"))))
       (format t "~%")))


(defun draw-dungeon (&optional (d *dungeon*) (c *camera*))
  (loop :for x :from 0 :to (1- (dungen:stage-width d)) :do
       (loop :for y :from 0 :to (1- (dungen:stage-height d)) :do
            (when (tile-in-view-p x y)
              (draw-dungeon-tile x y c)))))


(defun random-floor-cell (arr)
  (let* ((random-x (random (first  (array-dimensions arr))))
         (random-y (random (second (array-dimensions arr))))
         (random-cell (aref arr random-x random-y)))

    (if (not (member :wall (dungen::cell-features random-cell)))
        (aref arr random-x random-y)
        (random-floor-cell arr))))


(defun vec2-equal (vec1 vec2)
  (let ((vec1-x (truncate (gamekit:x vec1)))
        (vec1-y (truncate (gamekit:y vec1)))
        (vec2-x (truncate (gamekit:x vec2)))
        (vec2-y (truncate (gamekit:y vec2))))
    (if (and (= vec1-x vec2-x)
             (= vec1-y vec2-y))
        t
        nil)))


(defun increase-xp-threshold ()
  (incf *xp-til-next-lvl* (truncate (/ (* 3 *xp-til-next-lvl*) 2))))


(defun seq+ (&rest args)
  (if (null args)
      1
      (apply #'map (class-of (first args))
             #'+
             args)))


(defun seq* (&rest args)
  (if (null args)
      1
      (apply #'map (class-of (first args))
             #'*
             args)))


(defun seq- (&rest args)
  (apply #'map (class-of (first args))
         #'-
         args))


(defun seq/ (&rest args)
  (apply #'map (class-of (first args))
         #'/
         args))

(defun seq-floor (&rest args)
  (apply #'map (class-of (first args))
         #'floor
         args))


(defun get-random-room-tile (&optional (dungeon *dungeon*))
  (do ((temp-cell (random-floor-cell (dungen:stage-grid dungeon))
                  (random-floor-cell dungeon)))
         ((room-cell-p temp-cell) temp-cell)))


(defmacro not-zerop (number)
  `(not (zerop ,number)))


(defun room-cell-p (tile)
  (not-zerop (dungen::cell-region tile)))


(defun same-room-p (cell-1 cell-2)
  (let ((cell-1-region-id (dungen::cell-region cell-1))
        (cell-2-region-id (dungen::cell-region cell-2)))
    (if (= cell-1-region-id cell-2-region-id)
        t
        nil)))


(defun grid-pos->global-pos (vector)
  "`vector' is a 2D vector."
  (seq* vector (vector *tile-size* *tile-size*)))


(defun vector->vec2 (v)
  "Transforms a 2D vector into a gamekit:vec2."
  (gamekit:vec2 (elt v 0) (elt v 1)))


(defun initialize-dungeon ()
  (setf *xp-til-next-lvl* 10)
  (setf *camera* (make-camera 0 0 *window-width* *window-height*))
  (setf *dungeon* (dungen:make-stage :width        15
                                     :height       9
                                     :room-extent  3
                                     :door-rate    0.1
                                     :cycle-factor 0.2
                                     :density      0.3))

  (setf *player* (make-instance 'player :sprite *player-sprite*))
  (setf (global-pos *player*)
        (vector->vec2 (seq* (setf (player-grid-pos *player*) (calc-player-spawn-pos))
                            (vector *tile-size* *tile-size*))))

  (setf *enemy*  (make-instance 'enemy  :sprite *enemy-sprite*))
  (setf (global-pos *enemy*)
        (vector->vec2 (seq* (setf (enemy-grid-pos *enemy*) (calc-enemy-spawn-pos))
                            (vector *tile-size* *tile-size*))))
  (setf *game-over-p* nil))


(defun not-junction-p (cell)
  (not (member :junction (dungen::cell-features cell))))


(defun not-corrdor-p (cell)
  (not (member :corridor (dungen::cell-features cell))))

;;;; TIMERS
(defparameter *timers* nil)


(defun now ()
  (/ (get-internal-real-time) internal-time-units-per-second))


(defun timer-time (timer) (car timer))
(defun timer-handler (timer) (cdr timer))


(defun insert-timer (timer timers)
  "Inserts `timer' into the `timers' list, while making sure `timers' stays sorted.
Doesn't modify the original `timers' list."
  (if (endp timers)
      (list timer)
      (if (> (timer-time (first timers))
             (timer-time timer))
          (cons timer timers)
          (cons (first timers)
                (insert-timer timer (rest timers))))))


(defun add-timer (target handler)
  (setf *timers* (insert-timer (cons target handler) *timers*)))


(defun process-timers ()
  ;; this does not take advantage of *timers* being sorted.  For robustness. :D
  (let ((expired (find-if (lambda (timer)
                            (>= (now)
                                (car timer)))
                          *timers*)))
    (when expired
      (funcall (cdr expired))
      (setf *timers* (delete expired *timers*))
      (process-timers))))
