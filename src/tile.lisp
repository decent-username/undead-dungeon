;;;; tile.lisp
(in-package #:undead-dungeon)


;; tiles have a square shape
(defvar *dungeon-tile-size* 8)

(defun coordinates->global-pos (coordinates)
  (gamekit:mult coordinates *dungeon-tile-size*))
;;;; define-tiles

(defparameter *tile-stone-floor*
  (make-sprite-sheet-sprite 4 6 1 1 *dungeon-tile-size* :sprite-sheet))

(defparameter *tile-stone*
  (make-sprite-sheet-sprite 7 5 1 1 *dungeon-tile-size* :sprite-sheet))

(defparameter *tile-stone-1-3-6-8*
  (make-sprite-sheet-sprite 0 0 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-1-3-6*
  (make-sprite-sheet-sprite 1 0 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-1-3*
  (make-sprite-sheet-sprite 2 0 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-1-6*
  (make-sprite-sheet-sprite 3 0 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-1-8*
  (make-sprite-sheet-sprite 4 0 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-1*
  (make-sprite-sheet-sprite 5 0 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-2-4-5-7*
  (make-sprite-sheet-sprite 6 0 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-2-4-5*
  (make-sprite-sheet-sprite 7 0 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-2-4-7*
  (make-sprite-sheet-sprite 0 1 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-2-4*
  (make-sprite-sheet-sprite 1 1 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-2-5-7*
  (make-sprite-sheet-sprite 2 1 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-2-5*
  (make-sprite-sheet-sprite 3 1 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-2*
  (make-sprite-sheet-sprite 4 1 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-3-6*
  (make-sprite-sheet-sprite 5 1 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-3-8*
  (make-sprite-sheet-sprite 6 1 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-3*
  (make-sprite-sheet-sprite 7 1 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-4-5-7*
  (make-sprite-sheet-sprite 0 2 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-4-7*
  (make-sprite-sheet-sprite 1 2 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-4*
  (make-sprite-sheet-sprite 2 2 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-5-7*
  (make-sprite-sheet-sprite 3 2 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-5*
  (make-sprite-sheet-sprite 4 2 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-6-8*
  (make-sprite-sheet-sprite 5 2 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-6*
  (make-sprite-sheet-sprite 0 3 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-7*
  (make-sprite-sheet-sprite 7 2 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-8*
  (make-sprite-sheet-sprite 6 2 1 1 *dungeon-tile-size* :sprite-sheet))
;; new ones
(defparameter *tile-stone-2-4-8*
  (make-sprite-sheet-sprite 1 3 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-2-5-6*
  (make-sprite-sheet-sprite 2 3 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-2-7*
  (make-sprite-sheet-sprite 3 3 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-2-8*
  (make-sprite-sheet-sprite 4 3 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-2-6-8*
  (make-sprite-sheet-sprite 5 3 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-2-6*
  (make-sprite-sheet-sprite 6 3 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-4-5*
  (make-sprite-sheet-sprite 7 3 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-3-4-7*
  (make-sprite-sheet-sprite 0 4 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-3-4-8*
  (make-sprite-sheet-sprite 1 4 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-3-4*
  (make-sprite-sheet-sprite 2 4 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-1-5-7*
  (make-sprite-sheet-sprite 3 4 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-1-5-6*
  (make-sprite-sheet-sprite 4 4 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-1-5*
  (make-sprite-sheet-sprite 5 4 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-1-3-7*
  (make-sprite-sheet-sprite 6 4 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-1-3-8*
  (make-sprite-sheet-sprite 7 4 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-4-8*
  (make-sprite-sheet-sprite 0 5 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-1-7*
  (make-sprite-sheet-sprite 1 5 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-1-8*
  (make-sprite-sheet-sprite 2 5 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-1-6-8*
  (make-sprite-sheet-sprite 3 5 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-5-6*
  (make-sprite-sheet-sprite 4 5 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-3-7*
  (make-sprite-sheet-sprite 5 5 1 1 *dungeon-tile-size* :sprite-sheet))
(defparameter *tile-stone-3-6-8*
  (make-sprite-sheet-sprite 6 5 1 1 *dungeon-tile-size* :sprite-sheet))






;; Define tile
(defclass tile (positionable)
  ((grid-x :initarg :grid-x :accessor grid-x)
   (grid-y :initarg :grid-y :accessor grid-y)
   (sprite :initarg :sprite :accessor sprite))
  (:default-initargs
   :sprite (error "Every tile needs a sprite.")))


(defun make-tile (grid-x grid-y sprite)
  (make-instance 'tile
		 :grid-x grid-x
		 :grid-y grid-y
		 :sprite sprite))


(defmethod tile-grid-pos ((tile tile))
  (vector (grid-x tile) (grid-y tile)))


(defun tile-in-view-p (grid-x grid-y &optional (camera *camera*))
  "grid-pos is a 2D vector"
  (let* ((global-pos (tile-grid-pos->pixel-pos (vector grid-x grid-y)))
	 (global-x (gamekit:x global-pos))
	 (global-y (gamekit:y global-pos)))
    (and (>= global-x (camera-x camera))
	 (>= global-y (camera-y camera)))))


(defun draw-dungeon-tile (x y c &optional (grid (dungen:stage-grid *dungeon*)))
  "x y are grid positions that need to be translated
   to positions relative to the camera"
  ;; (format t "~&drawing tile: (~A/~A)~%" x y)
  (let* ((cell (aref grid x y))
	 (cell-features (dungen::cell-features cell))
	 (tile-sprite (resolve-tile-sprite cell-features))
	 (drawing-pos (tile-grid-pos->pixel-pos (vector x y))))
    (draw-sprite tile-sprite drawing-pos c)))


(defun resolve-tile-sprite (cell-features)

  (if (member :wall cell-features)
      (macrolet ((carved-at (idx) `(elt wall-feature-vec ,(1- idx))))
	;; select the correct wall tile
	(let ((wall-feature-vec (car cell-features)))
	  ;; (format t "~& wall-feature-vec: ~A~%" wall-feature-vec)
	  (cond
	    ;; 1
	    ((carved-at 1)
	     (cond ((carved-at 2)
		    (cond ((carved-at 4)
			   (cond ((carved-at 5)
				  (cond ((carved-at 7)
					 *tile-stone-2-4-5-7*)
					(t *tile-stone-2-4-5*)))
				 ((carved-at 7)
				  *tile-stone-2-4-7*)
				 ((carved-at 8)
				  *tile-stone-2-4-8*)
				 (t *tile-stone-2-4*)))

			  ((carved-at 5)
			   (cond ((carved-at 6)
				  (cond ((carved-at 7)
					 *tile-stone-2-5-7*)
					(t *tile-stone-2-5-6*)))
				 ((carved-at 7)
				  *tile-stone-2-5-7*)
				 (t *tile-stone-2-5*)))

			  ((carved-at 6)
			   (cond ((carved-at 7)
				  *tile-stone-2-7*)
				 ((carved-at 8)
				  *tile-stone-2-6-8*)
				 (t *tile-stone-2-6*)))

			  ((carved-at 7)
			   *tile-stone-2-7*)

			  ((carved-at 8)
			   *tile-stone-2-8*)
			  (t *tile-stone-2*)))

		   ((carved-at 3)
		    (cond ((carved-at 4)
			   (cond ((carved-at 5)
				  (cond ((carved-at 7)
					 *tile-stone-4-5-7*)
					(t *tile-stone-4-5*)))
				 ((carved-at 7)
				  *tile-stone-3-4-7*)
				 ((carved-at 8)
				  *tile-stone-3-4-8*)
				 (t *tile-stone-3-4*)))
			  ((carved-at 5)
			   (cond ((carved-at 6)
				  (cond ((carved-at 7)
					 *tile-stone-1-5-7*)
					(t *tile-stone-1-5-6*)))
				 ((carved-at 7)
				  *tile-stone-1-5-7*)
				 (t *tile-stone-1-5*)))
			  ((carved-at 6)
			   (cond ((carved-at 7)
				  *tile-stone-1-3-7*)
				 ((carved-at 8)
				  *tile-stone-1-3-6-8*)
				 (t *tile-stone-1-3-6*)))
			  ((carved-at 7)
			   *tile-stone-1-3-7*)
			  ((carved-at 8)
			   *tile-stone-1-3-8*)
			  (t *tile-stone-1-3*)))

		   ((carved-at 4)
		    (cond ((carved-at 5)
			   (cond ((carved-at 7)
				  *tile-stone-4-5-7*)
				 (t *tile-stone-4-5*)))
			  ((carved-at 7)
			   *tile-stone-4-7*)
			  ((carved-at 8)
			   *tile-stone-4-8*)
			  (t *tile-stone-4*)))
		   ((carved-at 5)
		    (cond ((carved-at 6)
			   *tile-stone-1-5-6*)
			  ((carved-at 7)
			   *tile-stone-1-5-7*)
			  (t *tile-stone-1-5*)))
		   ((carved-at 6)
		    (cond ((carved-at 7)
			   *tile-stone-1-7*)
			  ((carved-at 8)
			   *tile-stone-1-6-8*)
			  (t *tile-stone-1-6*)))
		   ((carved-at 7)
		    *tile-stone-1-7*)
		   ((carved-at 8)
		    *tile-stone-1-8*)
		   (t *tile-stone-1*)))
	    ;; 2
	    ((carved-at 2)
	     (cond ((carved-at 4)
		    (cond ((carved-at 5)
			   (cond ((carved-at 7)
				  *tile-stone-2-4-5-7*)
				 (t *tile-stone-2-4-5*)))
			  ((carved-at 7)
			   *tile-stone-2-4-7*)
			  ((carved-at 8)
			   *tile-stone-2-4-8*)
			  (t *tile-stone-2-4*)))
		   ((carved-at 5)
		    (cond ((carved-at 6)
			   (cond ((carved-at 7)
				  *tile-stone-2-5-7*)
				 (t *tile-stone-2-5-6*)))
			  ((carved-at 7)
			   *tile-stone-2-5-7*)
			  (t *tile-stone-2-5*)))
		   ((carved-at 6)
		    (cond ((carved-at 7)
			   *tile-stone-2-7*)
			  ((carved-at 8)
			   *tile-stone-2-6-8*)
			  (t *tile-stone-2-6*)))
		   ((carved-at 7)
		    *tile-stone-2-7*)
		   ((carved-at 8)
		    *tile-stone-2-8*)
		   (t *tile-stone-2*)))
	    ;; 3
	    ((carved-at 3)
	     (cond ((carved-at 4)
		    (cond ((carved-at 5)
			   (cond ((carved-at 7)
				  *tile-stone-4-5-7*)
				 (t *tile-stone-4-5*)))
			  ((carved-at 7)
			   *tile-stone-4-5-7*)
			  ((carved-at 8)
			   *tile-stone-3-4-8*)
			  (t *tile-stone-3-4*)))
		   ((carved-at 5)
		    (cond ((carved-at 6)
			   (cond ((carved-at 7)
				  *tile-stone-5-7*)
				 (t *tile-stone-5-6*)))
			  ((carved-at 7)
			   *tile-stone-5-7*)
			  (t *tile-stone-5*)))
		   ((carved-at 6)
		    (cond ((carved-at 7)
			   *tile-stone-3-7*)
			  ((carved-at 8)
			   *tile-stone-3-6-8*)
			  (t *tile-stone-3-6*)))
		   ((carved-at 7)
		    *tile-stone-3-7*)
		   ((carved-at 8)
		    *tile-stone-3-8*)
		   (t *tile-stone-3*)))
	    ;; 4
	    ((carved-at 4)
	     (cond ((carved-at 5)
		    (cond ((carved-at 7)
			   *tile-stone-4-5-7*)
			  (t *tile-stone-4-5*)))
		   ((carved-at 6)
		    (cond ((carved-at 7)
			   *tile-stone-4-7*)
			  ((carved-at 8)
			   *tile-stone-4-8*)
			  (t *tile-stone-4*)))
		   ((carved-at 7)
		    *tile-stone-4-7*)
		   ((carved-at 8)
		    *tile-stone-4-8*)
		   (t *tile-stone-4*)))
	    ;; 5
	    ((carved-at 5)
	     (cond ((carved-at 6)
		    (cond ((carved-at 7)
			   *tile-stone-5-7*)
			  (t *tile-stone-5-6*)))
		   ((carved-at 7)
		    *tile-stone-5-7*)
		   (t *tile-stone-5*)))
	    ;; 6
	    ((carved-at 6)
	     (cond ((carved-at 7)
		    *tile-stone-7*)
		   ((carved-at 8)
		    *tile-stone-6-8*)
		   (t *tile-stone-6*)))
	    ;; 7
	    ((carved-at 7)
	     *tile-stone-7*)
	    ;; 8
	    ((carved-at 8)
	     *tile-stone-8*)
	    (t *tile-stone*))))

      *tile-stone-floor*))
