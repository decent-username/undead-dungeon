;;;; camera.lisp
(in-package #:undead-dungeon)

(defclass camera ()
  ((x      :accessor camera-x      :initarg :x     )
   (y      :accessor camera-y      :initarg :y     )
   (width  :accessor camera-width  :initarg :width )
   (height :accessor camera-height :initarg :height)))


(defun make-camera (x y width height)
  (make-instance 'camera :x x :y y :width width :height height))


(defmethod camera-resolution ((c camera))
  "Return the width and height of the camera as a 2D vector."
  (list (camera-width c) (camera-height c)))


(defmethod camera-origin ((c camera))
  "Returns the cameras bottom left corner as a gamekit:2D vector."
  (gamekit:vec2 (camera-x c) (camera-y c)))


(defmethod (setf camera-origin) ((val gamekit:vec2) (c camera))
  "Changes the cameras bottom left corner to `val'.
  `val' needs to be a gamekit:vec2 with two elements."
  (setf (camera-x c) (gamekit:x val)
        (camera-y c) (gamekit:y val)))


(defmethod camera-center ((c camera))
  "Returns the camera center as a 2D vector relative to
   the bottom left corner of the camera."
  (gamekit:add (camera-origin c)
               (gamekit:vec2
                (/ (camera-width  c) 2)
                (/ (camera-height c) 2))))


(defmethod position-inside-camera ((c camera) (p gamekit:vec2))
  "From absolute position p to position relative to (inside) the camera
  (relative to the bottom left corner of the camera)
  `p' is a gamekit:vec2"
  (gamekit:subt p (camera-origin c)))


(defun slide-camera-towards! (camera position &optional (lerp-amount 1))
  "`position' is a gamekit:vec2 and defines the cameras new origin.
   This function calculates an inbetween point between the `camera' position
   and `position'. The specific point is decided by `lerp-amount'"
  (setf (camera-origin camera)
        (gamekit:vec2 (lerp (camera-x camera)
                            (gamekit:x position)
                            lerp-amount)
                      (lerp (camera-y camera)
                            (gamekit:y position)
                            lerp-amount))))


(defmethod print-object ((obj camera) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots ((x x)
                 (y y)
                 (width width)
                 (height height))
        obj
      (format stream "at (~A/~A) with w:~A and h:~A" x y width height))))


;;;; transformation functions
;;;;----------------------------------------------------------------------------
(defun tile-grid-pos->pixel-pos (position)
  "Calculates the position in pixels.
  `position' is a 2D vector

  returns: gamekit:vec2"
  (vector->vec2 (seq* position (vector *tile-size* *tile-size*))))


(defun pixel-pos->tile-grid-pos (pixel-pos)
  "`pixel-pos' is a gamekit:vec2"
  (vec2-floor (gamekit:div pixel-pos *dungeon-tile-size*)))


(defun move-camera! (camera direction &optional (amount 1))
  "`camera' is a camera objct,
   `direction' is one of these keywords: :up :down :left :right,
   `amount' is an integer that indicates how much the camera should be moved"

  (let ((direction-vec (calculate-direction-vec direction amount)))
   (setf (camera-origin camera)
         (gamekit:add (camera-origin camera)
                      direction-vec))))


(defun calculate-direction-vec (direction amount)
  (case direction
    (:up    (gamekit:vec2    0          amount))
    (:down  (gamekit:vec2    0       (- amount)))
    (:left  (gamekit:vec2 (- amount)    0))
    (:right (gamekit:vec2    amount     0))))
