;;;; player.lisp

(in-package #:undead-dungeon)

(defparameter *xp-til-next-lvl* 10)

(defparameter *player-sprite*
  (make-sprite-sheet-sprite 3 6  1 1
			    *dungeon-tile-size*
			    :sprite-sheet))

(defparameter *ferret-effect-sprite*
  (make-sprite-sheet-sprite 0 6  1 1
			    *dungeon-tile-size*
			    :sprite-sheet))

(defparameter *ferret*
  (make-sprite-sheet-sprite 1 6  1 1
			    *dungeon-tile-size*
			    :sprite-sheet))

(defclass player (positionable)
  ((health
    :accessor player-health
    :type integer
    :initform 20)
   (stamina
    :accessor player-stamina
    :type integer
    :initform 20)
   (physical-attack
    :accessor player-attack
    :type integer
    :initform 1)
   (physical-defense
    :accessor player-physical-defense
    :type integer
    :initform 1)
   (magic-attack
    :accessor player-magic-attack
    :type integer
    :initform 0)
   (magic-defense
    :accessor player-magic-defense
    :type integer
    :initform 1)
   (experience
    :accessor player-experience
    :type integer
    :initform 0)
   (level
    :accessor player-level
    :type integer
    :initform 1)
   (iventory
    :accessor player-inventory
    :type list
    :initform (list))
   (sprite
    :initarg :sprite
    :accessor player-sprite
    :initform (error "The player needs a sprite!"))
   (grid-pos
    :initarg :grid-pos
    :accessor player-grid-pos
    :initform (vector 0 0)
    :documentation "Needs to be a 2D vector.")))


(defmethod draw-player ((p player))
  (draw-sprite (player-sprite p)
	       (global-pos p)
	       *camera*))


(defmethod move-player! ((p player) direction-keyword)
  " :up :down :left :right"
  (unless *game-over-p*
    (with-accessors ((grid-pos player-grid-pos)
		     (global-pos global-pos)
		     (xp player-experience))
	p

      (when (= xp (1- *xp-til-next-lvl*))
	(gamekit:play-sound :level-up)
	(incf (player-level *player*))
	(increase-xp-threshold))


      (let ((new-pos (case direction-keyword
		       (:up    (seq+ grid-pos (vector  0  1)))
		       (:down  (seq+ grid-pos (vector  0 -1)))
		       (:left  (seq+ grid-pos (vector -1  0)))
		       (:right (seq+ grid-pos (vector  1  0))))))

	(let ((x (elt new-pos 0))
	      (y (elt new-pos 1)))

	  (unless (or (member :wall (dungen::cell-features
				     (aref (dungen::stage-grid *dungeon*) x y)))
		      (< x 0)
		      (< y 0))
	    ;; kill enemy if you step on him
	    (cond ((and (not (null *enemy*))
			(equalp new-pos (enemy-grid-pos *enemy*)))
		   (kill-enemy *enemy*)
		   (setf *game-over-p* t)
		   (add-timer (+ 2 (now)) (lambda () (initialize-dungeon))))
		  (t (gamekit:play-sound :step)
		     (incf xp)                  ;Yeah, walking improves your character!
		     (setf grid-pos new-pos)
		     (setf global-pos (vector->vec2 (seq* new-pos (vector *tile-size* *tile-size*))))))))))))


(defun calc-player-spawn-pos (&optional (player *player*))
  (loop :for x :from 0 :below (first (array-dimensions (dungen::stage-grid *dungeon*)))
     :do
       (loop :for y :from 0 :below (second (array-dimensions (dungen::stage-grid *dungeon*)))
	  :do
	    (let ((curr-cell (aref (dungen::stage-grid *dungeon*) x y)))
	      (when (and (not-zerop (dungen::cell-region curr-cell))
			 (not-corrdor-p  curr-cell)
			 (not-junction-p curr-cell))
	       (return-from calc-player-spawn-pos (vector x y)))))))
