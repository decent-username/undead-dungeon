;;;; package.lisp

(defpackage #:undead-dungeon
  (:use #:cl)
  (:export "START")
  (:export "STOP"))
