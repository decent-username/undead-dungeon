;;;; main.lisp

(in-package #:undead-dungeon)


;;;;-----------------------------------------------------------------------------
;;;; Constants
;;;;-----------------------------------------------------------------------------

;;;; Interface Variables
(defvar *window-scale* 6)

(defvar *tile-size* 8)

(defvar *window-width*  (* *window-scale* (* 15 *tile-size*)))
(defvar *window-height* (* *window-scale* (* 9  *tile-size*)))

(defvar *window-origin* (gamekit:vec2 0 0))

(defvar *window-center* (gamekit:vec2 (/ *window-width* 2)
                                      (/ *window-height* 2)))

;;;; Colors
(defvar *black* (gamekit:vec4 0 0 0 1))
(defvar *white* (gamekit:vec4 1 1 1 1))


;;;; Timer variables
(defparameter *dirty-last-repeat* 0)
(defparameter *button-repeat-delay* 0.01)
(defparameter *restart-game-time* 2)

;;;; Most important global Objects
(defparameter *camera* nil)
(defparameter *dungeon* nil)
(defparameter *player* nil)
(defparameter *enemy* nil)

;;;; Miscellaneous
(defvar *game-over-p* nil)

;;;;-----------------------------------------------------------------------------
;;;; Trivial-gamekit define game
;;;;-----------------------------------------------------------------------------

(gamekit:defgame undead-dungeon () ()
  (:viewport-width  *window-width*)
  (:viewport-height *window-height*)
  (:viewport-title "undead-dungeon"))


;; Register resource folder
(gamekit:register-resource-package :keyword (asdf:system-relative-pathname
                                             :undead-dungeon "assets/"))

(gamekit:define-image :sprite-sheet "img/sprite-sheet.png"
  :use-nearest-interpolation t)

(defmethod gamekit:post-initialize ((app undead-dungeon))

;;;;----------------------------------------------------------------------------
;;;; Resources
;;;;----------------------------------------------------------------------------
  (gamekit:define-sound :level-up "snd/sword-unsheathe3.wav")
  (gamekit:define-sound :step "snd/beads.wav")
  (gamekit:define-sound :kill "snd/random2.wav")


  ;; initialize dungeon, player, enemy and
  ;; whatever else is needed for the game to run
  (initialize-dungeon)


  ;; TODO: prepare resource and show loading screen

  ;; bind keys and potentially other things
  (gamekit:bind-cursor (lambda (x y)

                         nil))

  ;; up ----------------------------------------------------------------------------
  (gamekit:bind-button :up :pressed
   (lambda () (move-player! *player* :up)))
  (gamekit:bind-button :up :repeating
    (lambda () (when (< *button-repeat-delay* (- (now) *dirty-last-repeat*))
                 (setf *dirty-last-repeat* (now))
                 (move-player! *player* :up))))
  ;; down ----------------------------------------------------------------------------
  (gamekit:bind-button :down :pressed
   (lambda () (move-player! *player* :down)))

  (gamekit:bind-button :down :repeating
   (lambda () (when (< *button-repeat-delay* (- (now) *dirty-last-repeat*))
                (setf *dirty-last-repeat* (now))
                (move-player! *player* :down))))
  ;; left ----------------------------------------------------------------------------
  (gamekit:bind-button :left :pressed
   (lambda () (move-player! *player* :left)))
  (gamekit:bind-button :left :repeating
   (lambda () (when (< *button-repeat-delay* (- (now) *dirty-last-repeat*))
                (setf *dirty-last-repeat* (now))
                (move-player! *player* :left))))
  ;; right ----------------------------------------------------------------------------
  (gamekit:bind-button :right :pressed
   (lambda () (move-player! *player* :right)))
  (gamekit:bind-button :right :repeating
   (lambda () (when (< *button-repeat-delay* (- (now) *dirty-last-repeat*))
                (setf *dirty-last-repeat* (now))
                (move-player! *player* :right)))))


(defmethod gamekit:act ((app undead-dungeon))
  (process-timers))


(defmethod gamekit:draw ((app undead-dungeon))
  (unless *game-over-p*
    (draw-dungeon)
    (draw-player *player*)
    (draw-enemy *enemy*)

    (gamekit:draw-text (format nil "Player Health: ~A" (player-health *player*))
                       (gamekit:vec2 (/ *window-width* 20)
                                     (- *window-height* (/ *window-height* 10)))
                       :fill-color (gamekit:vec4 1 .2 .2 1))

    (gamekit:draw-text (format nil "lvl ~A [~A/~A]" (player-level *player*)
                               (player-experience *player*)
                               *xp-til-next-lvl*)
                       (gamekit:vec2 (/ *window-width* 20)
                                     (- *window-height* (/ *window-height* 6)))
                       :fill-color (gamekit:vec4 1 1 0 1)))

  (when *game-over-p*
    (gamekit:draw-rect *window-origin*
                       *window-width*
                       *window-height*
                       :fill-paint (gamekit:vec4 .1 .2 .07 1))

    (gamekit:draw-text "Congratulations! You won!"
                       (gamekit:subt *window-center* (gamekit:vec2 100 0))
                       :fill-color (gamekit:vec4 .5 .6 .3 1))))

;;;;-----------------------------------------------------------------------------
;;;; Exported Functions
;;;;-----------------------------------------------------------------------------
(defun start ()
  (gamekit:start 'undead-dungeon))

(defun stop ()
  (gamekit:stop))
