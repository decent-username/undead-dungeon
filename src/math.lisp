;;;; math.lisp
(cl:in-package #:undead-dungeon)

(defun center-of (rect)
  "Calculates coordinates of the middle position of `rect'
  `rect' is a gamekit:vec4 vector #(1 2 3 4)
				    ^ ^ ^ ^
				    x y z w
   and returns it as a 2D vector."
  (vec2
   (+ (gamekit:x rect) (/ (gamekit:z rect) 2))
   (+ (gamekit:y rect) (/ (gamekit:w rect) 2))))


(defun intersect-p (rect1 rect2)
  "Tests if the x of rect2 is inside the x-range of rect1 *and*
   tests if the y of rect2 is inside the y-range of rect1."
  (if (and
       (> (+ (x rect1) (z rect1)) (x rect2))
       (< (x rect1) (+ (x rect2) (z rect2))))
      (if (and
	   (> (+ (y rect1) (w rect1)) (y rect2))
	   (< (y rect1) (+ (y rect2) (w rect2))))
	  t)))


(defun distance (origin target)
  "Calculates the euclidean distance between
   the 2D vector `origin'and `target'."
  (let ((difference (subt origin target)))
    (sqrt (+
	   (* (gamekit:x difference) (gamekit:x difference))
	   (* (gamekit:y difference) (gamekit:y difference))))))


(defun lerp (a b c)
  "linear interpolation:
   Calculates a point between `a' and `b' based on `c'.
  `c' indicates how close to `b' the value should be.
   With c=1 this form evaluates to `b'."
  (+ (* a (- 1 c))
     (* c b)))
