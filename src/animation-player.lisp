;;;; animation-player.lisp
(in-package #:undead-dungeon)



(defparameter *player-idle-west*
  (make-animation '(((* 3 8) (* 0 8) (* 4 8) (* 1 8) 0
                     nil nil))
                  0 :looped-p nil))


(defparameter *player-walking-east*
  (make-animation '(((* 0 64) (* 9 64) (* 1 64) (* 10 64) 0
                     nil nil)
                    ((* 1 64) (* 9 64) (* 2 64) (* 10 64) 0.125
                     nil nil)
                    ((* 2 64) (* 9 64) (* 3 64) (* 10 64) 0.25
                     nil nil)
                    ((* 3 64) (* 9 64) (* 4 64) (* 10 64) 0.375
                     nil nil)
                    ((* 4 64) (* 9 64) (* 5 64) (* 10 64) 0.5
                     nil nil)
                    ((* 5 64) (* 9 64) (* 6 64) (* 10 64) 0.625
                     nil nil)
                    ((* 6 64) (* 9 64) (* 7 64) (* 10 64) 0.75
                     nil nil)
                    ((* 7 64) (* 9 64) (* 8 64) (* 10 64) 0.875
                     nil nil)
                    ((* 8 64) (* 9 64) (* 9 64) (* 10 64) 1.0
                     nil nil))
                  1.125 :looped-p t))
