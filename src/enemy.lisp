;;;; enemy.lisp
(in-package #:undead-dungeon)

(defparameter *enemy-sprite*
  (make-sprite-sheet-sprite 2 6 1 1
			    *dungeon-tile-size*
			    :sprite-sheet))


(defclass enemy (positionable)
  ((hp
    :accessor enemy-hp
    :initarg  :hp)
   (hp-max
    :accessor enemy-hp-max
    :initarg  :hp-max)
   (mp
    :accessor enemy-mp
    :initarg  :mp)
   (mp-max
    :accessor enemy-mp-max
    :initarg  :mp-max)
   (agility
    :accessor enemy-agility
    :initarg  :agility)
   (physical-attack
    :accessor enemy-physical-attack
    :initarg  :physical-attack)
   (physical-defense
    :accessor enemy-physical-defense
    :initarg  :physical-defense)
   (magical-attack
    :accessor enemy-magic-attack
    :initarg  :magic-attack)
   (magical-defense
    :accessor enemy-magic-defense
    :initarg  :magic-defense)
   (vulnerabilities
    :accessor enemy-vulnerabilities
    :initarg  :vulnerabilities)
   (grid-pos
    :accessor enemy-grid-pos
    :initarg :grid-pos
    :initform (vector 0 0))
   (sprite
    :initarg :sprite
    :accessor enemy-sprite
    :initform (error "Every enemy needs a sprite!"))))


(defun draw-enemy (e)
  (unless (null e)
   (draw-sprite (enemy-sprite e)
		(global-pos e)
		*camera*)))


(defun kill-enemy (e)
  (gamekit:play-sound :kill)
  (setf *enemy* nil)

  (when (> (+ (player-experience *player*) 100) *xp-til-next-lvl*)
      (increase-xp-threshold)
      (incf (player-experience *player*) 100)))


(defun calc-enemy-spawn-pos (&optional (enemy *enemy*))
  (let* ((spawn-tile (get-random-room-tile))
	 (spawn-pos (vector (dungen::cell-x spawn-tile)
				  (dungen::cell-y spawn-tile))))
    (if (spawn-pos-valid-p spawn-pos)
	spawn-pos
	(calc-enemy-spawn-pos))))


(defun spawn-pos-valid-p (pos)
  ;; A spawn position is valid if the `pos' is in different room than the player.
  ;; `pos' is a 2D vector containing the positions x and y
  (let* ((player-pos (player-grid-pos *player*))
	 (player-pos-x (elt player-pos 0))
	 (player-pos-y (elt player-pos 1))
	 (player-pos-cell (aref (dungen:stage-grid *dungeon*)
				player-pos-x
				player-pos-y))
	 (pos-cell (aref (dungen:stage-grid *dungeon*)
			 (elt pos 0)
			 (elt pos 1))))
    ;; player needs to be spawned first
    (and (not (same-room-p player-pos-cell pos-cell))
	 (not-corrdor-p pos-cell)
	 (not-junction-p pos-cell))))
